import uvicorn
from fastapi import FastAPI
from fastapi.middleware.wsgi import WSGIMiddleware
from typing import Optional
from dash_clinical_analytics.app import create_dash_app
from pydantic import BaseModel
import pickle
import numpy as np


# Class which describes a single patient's measurements
class PatientData(BaseModel):
    """
    A class used to represent a patients data

    ...

    Attributes
    ----------
    pregnancies : int
        Number of times pregnant
    plasma_glucose : int
        Plasma glucose concentration a 2 hours in an oral glucose tolerance test
    blood_pressure : int
        Diastolic blood pressure (mm Hg)
    skin_fold : int
        Triceps skin fold thickness (mm))
    serum_insuline: float
        2-Hour serum insulin (mu U/ml)
    bmi: float
        Body mass index (weight in kg/(height in m)^2)
    pedigree_function: float
        Diabetes pedigree function
    age: int
        Age (years)
    """

    pregnancies: int
    plasma_glucose: int
    blood_pressure: int
    skin_fold: int
    serum_insuline: float
    bmi: float
    pedigree_function: float
    age: int


app = FastAPI()

# Load the pre trained model
filename = "finalized_model.sav"
loaded_model = pickle.load(open(filename, "rb"))

# Use @app.get() decorator to define a GET request endpoint with the "main status and predict of the api"
@app.get("/api")
def read_main():
    return {
        "routes": [
            {"method": "GET", "path": "/api/status", "summary": "App status"},
            {"method": "POST", "path": "/api/predict", "predict": "Get prediction"},
        ]
    }

# Api status exposed on the /api/status endpoint
@app.get("/api/status")
def get_status():
    return {"status": "ok"}

# Use @app.post() decorator to define a POST request endpoint that lets passing a body on the request and returns a json
@app.post("/api/predict")
def predict_diabetes(patient: PatientData):

    # Recieve the POST request body and get the dictionary object
    data = patient.dict()

    # In order for the predict method to work we need a "column" array
    data_ready = np.array(list(data.values())).reshape(1, -1)

    # Load the model and use the predict method

    prediction = loaded_model.predict(data_ready)

    probability = zip(
        [str(int(classes)) for classes in loaded_model.classes_],
        loaded_model.predict_proba(data_ready)[0],
    )

    probability = dict(probability)

    return {"prediction": prediction[0], "probability": probability}


# Mounting the app to root and allowing the Dash/Flask app to prefix itself

dash_app = create_dash_app(routes_pathname_prefix="/")
app.mount("/", WSGIMiddleware(dash_app.server))


if __name__ == "__main__":

    # Run the app with uvicorn ASGI server asyncio frameworks. That basically responds to request on parallel and faster

    uvicorn.run("api_app:app", host="0.0.0.0", port=8000, reload=True)
