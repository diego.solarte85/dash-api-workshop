DS4A Cohort 5 - How to add an API to a Dash Plotly application.

Tech used:

1. FastAPI:  https://fastapi.tiangolo.com/
2. uvicorn: https://www.uvicorn.org/
3. Plotly-Dash: https://plotly.com/dash/